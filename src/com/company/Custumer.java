package com.company;

public class Custumer {
    private String name;
    private double cashOnHand;
    private String address;

    public Custumer() {

    }

    public Custumer(String name, double cashOnHand, String address) {
        this.name = name;
        this.cashOnHand = cashOnHand;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCashOnHand() {
        return cashOnHand;
    }

    public void setCashOnHand(double cashOnHand) {
        this.cashOnHand = cashOnHand;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void purchaseCar(Vehichle vehichle, Emploee emploee, boolean finance){
        emploee.handleCustumer(finance,this,vehichle);
    }
}
