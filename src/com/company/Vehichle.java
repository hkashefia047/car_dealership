package com.company;

public class Vehichle {
    private double price;
    private String model;
    private String name;

    public Vehichle() {
    }

    public Vehichle(int price, String model, String name) {
        this.price = price;
        this.model = model;
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Vehichle{" +
                "price=" + price +
                ", model='" + model + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
